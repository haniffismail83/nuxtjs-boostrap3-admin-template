export const state = () => ({
  counter: 0,
  sidemenu: 'active'
})

export const mutations = {
  toggleSideMenu (state) {
    console.log('store toggle called')
    if (state.sidemenu === 'active') {
      state.sidemenu = 'active-sm'
    } else {
      state.sidemenu = 'active'
    }
    console.log(state.sidemenu)
  }
}
